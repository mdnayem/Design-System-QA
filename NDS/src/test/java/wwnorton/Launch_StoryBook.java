package wwnorton;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Launch_StoryBook {
	
	public static void main(String[] args)
	{
		
		System.setProperty("webdriver.chrome.driver","D:\\SW Installation\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://wwnorton.github.io/design-system/");
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//a[@href=\"https://wwnorton.github.io/design-system/storybook\"]")).click();

	}
	

}
