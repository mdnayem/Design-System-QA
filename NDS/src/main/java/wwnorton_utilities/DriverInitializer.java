package wwnorton_utilities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverInitializer {
	
	//Invoking Driver
	
public static WebDriver driver;
	
	static{
	
		System.setProperty("webdriver.chrome.driver","D:\\SW Installation\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://wwnorton.github.io/design-system/storybook/?path=/story/badge--default");
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	}
	
	public DriverInitializer() {}

}
